from game_utils import negamax
from model import Magikarp
import chess
import numpy as np
import tensorflow as tf
from pygame.locals import MOUSEBUTTONDOWN, MOUSEBUTTONUP
from ChessPieces import *

"""
	pystockfish
	~~~~~~~~~~~~~~~

	Wraps the Stockfish chess engine.  Assumes stockfish is
	executable at the root level.

	Built on Ubuntu 12.1 tested with Stockfish 120212.

	:copyright: (c) 2013 by Jarret Petrillo.
	:license: GNU General Public License, see LICENSE for more details.
"""

import re
import subprocess
from random import randint

MAX_MOVES = 200
UCI_MOVE_REGEX = "[a-h]\d[a-h]\d[qrnb]?"
PV_REGEX = " pv (?P<move_list>{0}( {0})*)".format(UCI_MOVE_REGEX)



cur = {}
myPieces = []


def showCoordinates():
	for key, value in cur.items():
		current = value
		print(key + " = " + str(current) + " " + str(current.center))


def isTheSame(pos1, pos2):
	# print("got as " + str(pos1) + " " + str(pos2))
	return abs(pos1[0] - pos2[0]) <= 150 and abs(pos1[1] - pos2[1]) <= 150

def getTheNearest(pos):
	dist = 10000000
	nearest = None
	# print("GOT " + str(pos))

	for i in range(0, len(Pieces)):
		# cur = Pieces[i].square.center
		current = Pieces[i].square.center
		coord = (current[1] - 40, current[0] - 40)
		if (coord[0] == pos[0] and coord[1] == pos[1]):
			nearest = Pieces[i]
			break
		# if (abs(coord[0] - pos[0]) <= 30 and abs(coord[1] - pos[1]) <= 30):
		# 	nearest = Pieces[i]
		# 	break



	return nearest

def getNearestV2(pos):
	print("in getNearestV2 " + str(pos))
	nearest = None
	for i in range(0, len(Pieces)):
		current = Pieces[i].square.center
		if current[0] == pos[0] and current[1] == pos[1]:
			nearest = Pieces[i]
			break
	return nearest


def moveWithUciV2(uci):

	f1 = cur[uci[0:2]]
	f2 = cur[uci[2:]]


	fromSquare = (f1.center[1], f1.center[0])
	toSquare = (f2.center[1], f2.center[0])

	print("fromSquare " + str(fromSquare))
	print("toSquare " + str(toSquare))

	showPieces()
	showCoordinates()


	piece = getNearestV2(fromSquare)
	if piece is None:
		print("There is no piece at " + str(uci[0:2]))
		print("!@(&#!*"*100)
		return


	toPiece = getNearestV2(toSquare)
	if toPiece is not None:
		print("found piece " + str(toPiece.square) + " " + str(toPiece.square.center))
		print("REMOVING toPiece")
		print(toPiece.team)
		print(len(Pieces))
		Pieces.remove(toPiece)
		print(len(Pieces))
		pygame.display.flip()





	# cell1 = cur["a1"]
	cell1 = piece.square
	cell1[0] = toSquare[0] - 40
	cell1[1] = toSquare[1] - 40


	# piece.update(cell1)
	piece.square = cell1
	piece.rect.center = toSquare


	print("updating rect to " + str(piece.square))
	print("updating rect center to " + str(toSquare))


	updateBoard()




def moveUCI(uci):
	fromSquare = cur[uci[0:2]]
	# fromSquare = (cur[uci[0:2]].center[1], cur[uci[0:2]].center[0])
	# temp = fromSquare[0]
	# fromSquare[0] = fromSquare[1]
	# fromSquare


	# toSquare = (cur[uci[2:]][1] + 40, cur[uci[2:]][0] + 40)
	toSquare = cur[uci[2:]]
	# print("toSQUARE " + str(toSquare))

	temp = toSquare[0]
	toSquare[0] = toSquare[1] + 40
	toSquare[1] = temp + 40
	#
	# print("toSQUARE " + str(toSquare))

	print("from " + str(fromSquare))
	print("to " + str(toSquare))
	# for i in range(0, len(Pieces)):
	#     print(str(Pieces[i].square.center) + " " + Pieces[i].team + " " + str(Pieces[i].rect))

	piece = getTheNearest(fromSquare)


	#remove before placing
	toSquare[0] -= 40
	toSquare[1] -= 40

	toPiece = getTheNearest(toSquare)
	if toPiece is not None:
		print("There is a PIECE!!")
		Pieces.remove(toPiece)
	else:
		print("There is no piece")


	# piece = None


	# toSquare[0] -= 40
	# toSquare[1] -= 45
	print("before " + str(piece.square.center))
	piece.update(toSquare)
	print("after " + str(piece.square.center))

	# if (piece is not None):
	# 	piece.update(toSquare)


	updateBoard()
	# showPieces()


class Match:
	"""
	The Match class setups a chess match between two specified engines.  The white player
	is randomly chosen.

	deep_engine = Engine(depth=20)
	shallow_engine = Engine(depth=10)
	engines = {
		'shallow': shallow_engine,
		'deep': deep_engine,
		}

	m = Match(engines=engines)

	m.move() advances the game by one move.

	m.run() plays the game until completion or 200 moves have been played,
	returning the winning engine name.
	"""

	def __init__(self, engines):
		random_bin = randint(0, 1)
		self.white = list(engines.keys())[random_bin]
		self.black = list(engines.keys())[not random_bin]
		self.white_engine = engines.get(self.white)
		self.black_engine = engines.get(self.black)
		self.moves = []
		self.white_engine.newgame()
		self.black_engine.newgame()
		self.winner = None
		self.winner_name = None

	def move(self):
		"""
		Advance game by single move, if possible.

		@return: logical indicator if move was performed.
		"""
		if len(self.moves) == MAX_MOVES:
			return False
		elif len(self.moves) % 2:
			active_engine = self.black_engine
			active_engine_name = self.black
			inactive_engine = self.white_engine
			inactive_engine_name = self.white
		else:
			active_engine = self.white_engine
			active_engine_name = self.white
			inactive_engine = self.black_engine
			inactive_engine_name = self.black
		active_engine.setposition(self.moves)
		movedict = active_engine.bestmove()
		bestmove = movedict.get('move')
		info = movedict.get('info')
		ponder = movedict.get('ponder')
		self.moves.append(bestmove)

		if info["score"]["eval"] == "mate":
			matenum = info["score"]["value"]
			if matenum > 0:
				self.winner_engine = active_engine
				self.winner = active_engine_name
			elif matenum < 0:
				self.winner_engine = inactive_engine
				self.winner = inactive_engine_name
			return False

		if ponder != '(none)':
			return True

	def run(self):
		"""
		Returns the winning chess engine or "None" if there is a draw.
		"""
		while self.move():
			pass
		return self.winner


class Engine(subprocess.Popen):
	"""
	This initiates the Stockfish chess engine with Ponder set to False.
	'param' allows parameters to be specified by a dictionary object with 'Name' and 'value'
	with value as an integer.

	i.e. the following explicitly sets the default parameters
	{
		"Contempt Factor": 0,
		"Min Split Depth": 0,
		"Threads": 1,
		"Hash": 16,
		"MultiPV": 1,
		"Skill Level": 20,
		"Move Overhead": 30,
		"Minimum Thinking Time": 20,
		"Slow Mover": 80,
	}

	If 'rand' is set to False, any options not explicitly set will be set to the default
	value.

	-----
	USING RANDOM PARAMETERS
	-----
	If you set 'rand' to True, the 'Contempt' parameter will be set to a random value between
	'rand_min' and 'rand_max' so that you may run automated matches against slightly different
	engines.
	"""

	def __init__(self, depth=2, ponder=False, param={}, rand=False, rand_min=-10, rand_max=10):
		subprocess.Popen.__init__(self,
								  'stockfish',
								  universal_newlines=True,
								  stdin=subprocess.PIPE,
								  stdout=subprocess.PIPE, )
		self.depth = str(depth)
		self.ponder = ponder
		self.put('uci')
		if not ponder:
			self.setoption('Ponder', False)

		base_param = {
			"Write Debug Log": "false",
			"Contempt Factor": 0,  # There are some stockfish versions with Contempt Factor
			"Contempt": 0,  # and others with Contempt. Just try both.
			"Min Split Depth": 0,
			"Threads": 1,
			"Hash": 16,
			"MultiPV": 1,
			"Skill Level": 20,
			"Move Overhead": 30,
			"Minimum Thinking Time": 20,
			"Slow Mover": 80,
			"UCI_Chess960": "false",
		}

		if rand:
			base_param['Contempt'] = randint(rand_min, rand_max),
			base_param['Contempt Factor'] = randint(rand_min, rand_max),

		base_param.update(param)
		self.param = base_param
		for name, value in list(base_param.items()):
			self.setoption(name, value)

	def newgame(self):
		"""
		Calls 'ucinewgame' - this should be run before a new game
		"""
		self.put('ucinewgame')
		self.isready()

	def put(self, command):
		self.stdin.write(command + '\n')
		self.stdin.flush()

	def flush(self):
		self.stdout.flush()

	def setoption(self, optionname, value):
		self.put('setoption name %s value %s' % (optionname, str(value)))
		stdout = self.isready()
		if stdout.find('No such') >= 0:
			print("stockfish was unable to set option %s" % optionname)

	def setposition(self, moves=[]):
		"""
		Move list is a list of moves (i.e. ['e2e4', 'e7e5', ...]) each entry as a string.  Moves must be in full algebraic notation.
		"""
		self.put('position startpos moves %s' % Engine._movelisttostr(moves))
		self.isready()

	def setfenposition(self, fen):
		"""
		set position in fen notation.  Input is a FEN string i.e. "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1"
		"""
		self.put('position fen %s' % fen)
		self.isready()

	def go(self):
		self.put('go depth %s' % self.depth)

	@staticmethod
	def _movelisttostr(moves):
		"""
		Concatenates a list of strings.

		This is format in which stockfish "setoption setposition" takes move input.
		"""
		return ' '.join(moves)

	def bestmove(self):
		"""
		Get proposed best move for current position.

		@return: dictionary with 'move', 'ponder', 'info' containing best move's UCI notation,
		ponder value and info dictionary.
		"""
		self.go()
		last_info = ""
		last_score = -999
		while True:
			text = self.stdout.readline().strip()
			split_text = text.split(' ')
			# print(text)
			if split_text[0] == "info":
				last_info = Engine._bestmove_get_info(text)
				last_score = last_info['score']['value']
				# print(last_score)
			if split_text[0] == "bestmove":
				# print("last score is " + str(last_score))
				# ponder = None if len(split_text[0]) < 3 else split_text[3]
				return {'move': split_text[1],
						'score': last_score,
						'info': last_info}

	@staticmethod
	def _bestmove_get_info(text):
		"""
		Parse stockfish evaluation output as dictionary.

		Examples of input:

		"info depth 2 seldepth 3 multipv 1 score cp -656 nodes 43 nps 43000 tbhits 0 \
		time 1 pv g7g6 h3g3 g6f7"

		"info depth 10 seldepth 12 multipv 1 score mate 5 nodes 2378 nps 1189000 tbhits 0 \
		time 2 pv h3g3 g6f7 g3c7 b5d7 d1d7 f7g6 c7g3 g6h5 e6f4"
		"""
		result_dict = Engine._get_info_pv(text)
		result_dict.update(Engine._get_info_score(text))

		single_value_fields = ['depth', 'seldepth', 'multipv', 'nodes', 'nps', 'tbhits', 'time']
		for field in single_value_fields:
			result_dict.update(Engine._get_info_singlevalue_subfield(text, field))

		return result_dict

	@staticmethod
	def _get_info_singlevalue_subfield(info, field):
		"""
		Helper function for _bestmove_get_info.

		Extracts (integer) values for single value fields.
		"""
		search = re.search(pattern=field + " (?P<value>\d+)", string=info)
		return {field: int(search.group("value"))}

	@staticmethod
	def _get_info_score(info):
		"""
		Helper function for _bestmove_get_info.

		Example inputs:

		score cp -100        <- engine is behind 100 centipawns
		score mate 3         <- engine has big lead or checkmated opponent
		"""
		search = re.search(pattern="score (?P<eval>\w+) (?P<value>-?\d+)", string=info)
		return {"score": {"eval": search.group("eval"), "value": int(search.group("value"))}}

	@staticmethod
	def _get_info_pv(info):
		"""
		Helper function for _bestmove_get_info.

		Extracts "pv" field from bestmove's info and returns move sequence in UCI notation.
		"""
		search = re.search(pattern=PV_REGEX, string=info)
		return {"pv": search.group("move_list")}

	def isready(self):
		"""
		Used to synchronize the python engine object with the back-end engine.  Sends 'isready' and waits for 'readyok.'
		"""
		self.put('isready')
		while True:
			text = self.stdout.readline().strip()
			if text == 'readyok':
				return text


def quick_checkmate_test():
	# 1. e4 e5 2. Bc4 Nc6 3. Qf3 d6
	e1 = Engine(depth=6)
	e2 = Engine(depth=6)

	# match creation must be before calling setposition as Match.__init__ resets position
	m = Match(engines={"e1": e1, "e2": e2})

	e1.setposition(['e2e4', 'e7e5', 'f1c4', 'b8c6', 'd1f3', 'd7d6'])
	e2.setposition(['e2e4', 'e7e5', 'f1c4', 'b8c6', 'd1f3', 'd7d6'])

	m.run()
	assert m.winner in {"e1", "e2"}








config = {}
config['batch_size'] = 20
config['datafile'] = '../Data/training_data.hdf5'
config['p_datafile'] = '../Data/player_data.hdf5'
config['full_boards_file'] = '../Data/full_boards.pkl'
config['num_epochs'] = 1
config['save_file'] = 'trained_model/trained_genadv.ckpt'



deep = Engine(depth=10)

def getStockfishEval(moves):
	print("GOT ")
	print(moves)
	# deep.setposition(['e2e4', 'e7e5', 'f1a6', 'b7a6', 'g1f3'])
	deep.setposition(moves)
	res = deep.bestmove()
	# print(res)
	return res



allMoves = []
MAX_CENTIPAWN_DIFF = 80
MAX_STOCKFISH_DEPTH = 4

# allMoves.append('e2e4')
# allMoves.append('e7e5')
# allMoves.append('g1f3')
# allMoves.append('g8f7')
# res = getStockfishEval(allMoves)
# print(res)


pygame.display.set_caption('Askhat STGAN game vs Magnus Carlsen Model')
FPS = pygame.time.Clock()
FPS.tick(10)
drawboard(colors)
# for piece in Pieces:
# 	piece.draw(screen)



def myGame():
	# drawboard(colors)

	while True:
		updateBoard()
		pygame.display.flip()
		move = input()
		# showPieces()
		# print("!"*100)
		moveWithUciV2(move)
		# showPieces()
		# print("END"*40)

# myGame()


def updateBoard():
	drawboard(colors)
	for piece in myPieces:
		piece.draw(screen)
	pygame.display.flip()
initMap(cur)



# showCoordinates()
# showPieces()
# moveWithUciV2('a2a8')
# moveWithUciV2('b2b8')
# moveWithUciV2('c2c8')
# moveWithUciV2('d2d8')
# moveWithUciV2('e2e8')
# moveWithUciV2('f2f8')
# moveWithUciV2('h2h8')
# moveWithUciV2('g2g8')




##testing


# pawn = Pawn('MEDIA/WhitePawn.png', squareCenters[48], 'White')
# myPieces.append(pawn)

board = chess.Board()

def createAndDrawBoard(curBoard):
	print("GOT BOARD")
	myBoard = str(curBoard)
	# print("myBoard \n" + myBoard)
	cnt = 0
	for i in range(0, len(myBoard)):
		if (not myBoard[i].isalpha() and not myBoard[i] == '.'):
			continue
		# print(myBoard[i] + " " + str(cnt))

		if (myBoard[i] == 'P'):
			myPieces.append(Pawn('MEDIA/WhitePawn.png', squareCenters[cnt], 'White'))
			# print("got P " + str(cnt))
		if (myBoard[i] == 'p'):
			myPieces.append(BlackPawn('MEDIA/BlackPawn.png', squareCenters[cnt], 'Black'))

		if (myBoard[i] == 'r'):
			myPieces.append(Rook('MEDIA/BlackRook.png', squareCenters[cnt], 'Black'))
		if (myBoard[i] == 'R'):
			myPieces.append(Rook('MEDIA/WhiteRook.png', squareCenters[cnt], 'White'))

		if (myBoard[i] == 'n'):
			myPieces.append(Knight('MEDIA/BlackKnight.png', squareCenters[cnt], 'Black'))
		if (myBoard[i] == 'N'):
			myPieces.append(Knight('MEDIA/WhiteKnight.png', squareCenters[cnt], 'White'))

		if (myBoard[i] == 'b'):
			myPieces.append(Bishop('MEDIA/BlackBishop.png', squareCenters[cnt], 'Black'))
		if myBoard[i] == 'B':
			myPieces.append(Bishop('MEDIA/WhiteBishop.png', squareCenters[cnt], 'White'))

		if myBoard[i] == 'q':
			myPieces.append(Queen('MEDIA/BlackQueen.png', squareCenters[cnt], 'Black'))
		if myBoard[i] == 'Q':
			myPieces.append(Queen('MEDIA/WhiteQueen.png', squareCenters[cnt], 'White'))

		if myBoard[i] == 'K':
			myPieces.append(King('MEDIA/WhiteKing.png', squareCenters[cnt], 'White'))
		if myBoard[i] == 'k':
			myPieces.append(King('MEDIA/BlackKing.png', squareCenters[cnt], 'Black'))
		cnt += 1
	updateBoard()





# createAndDrawBoard(board)
# myGame()





# myGame()
# moveUCI("e2e4")

# print("-" * 100)
# moveUCI("d2d4")
# moveUCI("e2e4")
# print("Finished")
# showPieces()


# showCoordinates()
# myGame()



with tf.Session() as sess:
	board = chess.Board()
	myPieces = []
	createAndDrawBoard(board)

	# Load evaluation model
	magikarp = Magikarp(config, sess)
	magikarp.load_model(magikarp.save_file)


	while not board.is_checkmate():

		print('-' * 100)
		print("Current Board:\n\n", board, "\n")
		myPieces = []
		createAndDrawBoard(board)

		move = "a1a1"
		while True:
			raw_move = input("Please enter a move in UCI notation: ")
			if len(raw_move) != 4:
				print("Please use UCI notation!")
				continue
			move = chess.Move.from_uci(raw_move)
			if move in board.legal_moves:
				board.push(move)
				# add a move here
				allMoves.append(raw_move)
				break
			else:
				print("Please enter a valid move.")

		# moveUCI(raw_move)
		myPieces = []
		createAndDrawBoard(board)

		if board.is_checkmate():
			print("Congrats - you've just won!")
			break

		# Computer response
		stockfish = getStockfishEval(allMoves)

		# get evaluation of a stockfish
		stockFishMove = stockfish['move']
		print("stockFishMove " + str(stockFishMove))
		allMoves.append(stockFishMove)
		stockfishScore = getStockfishEval(allMoves)['score']
		allMoves.pop()

		# get evaluation of a STGAN
		score, comp_move = negamax(board, 0, -1, float('-inf'), float('inf'), magikarp)
		print("STGAN's score of a position is for white's favor " + str(score))

		allMoves.append(str(comp_move)[0:4])
		stockfishGANScore = getStockfishEval(allMoves)['score']
		allMoves.pop()

		print("Stockfish SCORE is (WHITE) " + str(stockfishScore))
		print("Stockfish GAN SCORE is (WHITE) " + str(stockfishGANScore))
		myPieces = []
		createAndDrawBoard(board)
		if stockfishScore < stockfishGANScore and stockfishGANScore - stockfishScore > MAX_CENTIPAWN_DIFF:
			print("The difference is higher than the max centipawn diff!!!!!! ")
			# print("Adjusting a move..." + " ADJUST! " * 10)
			print("Best move is " + str(stockFishMove) + " " + "#" * 20)
			allMoves.append(stockFishMove)
			# moveUCI(stockFishMove)
			myPieces = []
			createAndDrawBoard(board)
			board.push(chess.Move.from_uci(stockFishMove))
		else:
			print("GAN's score is accurate! ")
			allMoves.append(str(comp_move)[0:4])
			print("BEST MOVE IS " + str(str(comp_move)[0:4]) + " " + "$" * 20)
			# moveUCI(str(str(comp_move)[0:4]))
			board.push(comp_move)
			myPieces = []
			createAndDrawBoard(board)
		myPieces = []
		createAndDrawBoard(board)
































# with tf.Session() as sess:
# 	# Set up chess board
# 	board = chess.Board()
#
# 	# Load evaluation model
# 	magikarp = Magikarp(config, sess)
# 	magikarp.load_model(magikarp.save_file)
#
# 	# Begin chess game
# 	while not board.is_checkmate():
# 		# Human plays as white for simplicity
# 		print('-' * 100)
# 		print("Current Board:\n\n", board, "\n")
#
# 		move = "a1a1"
# 		while True:
# 			raw_move = input("Please enter a move in UCI notation: ")
# 			if len(raw_move) != 4:
# 				print("Please use UCI notation!")
# 				continue
# 			move = chess.Move.from_uci(raw_move)
# 			if move in board.legal_moves:
# 				board.push(move)
# 				#add a move here
# 				allMoves.append(raw_move)
# 				break
# 			else:
# 				print("Please enter a valid move.")
#
# 		if board.is_checkmate():
# 			print("Congrats - you've just won!")
# 			break
#
# 		#Computer response
# 		stockfish = getStockfishEval(allMoves)
#
# 		#get evaluation of a stockfish
# 		stockFishMove = stockfish['move']
# 		print("stockFishMove " + str(stockFishMove))
# 		allMoves.append(stockFishMove)
# 		stockfishScore = getStockfishEval(allMoves)['score']
# 		allMoves.pop()
#
#
# 		# get evaluation of a STGAN
# 		score, comp_move = negamax(board, 0, -1, float('-inf'), float('inf'), magikarp)
# 		print("STGAN's score of a position is for white's favor " + str(score))
#
# 		allMoves.append(str(comp_move)[0:4])
# 		stockfishGANScore = getStockfishEval(allMoves)['score']
# 		allMoves.pop()
#
#
#
# 		print("Stockfish SCORE is (WHITE) " + str(stockfishScore))
# 		print("Stockfish GAN SCORE is (WHITE) " + str(stockfishGANScore))
#
# 		if stockfishScore < stockfishGANScore and stockfishGANScore - stockfishScore > MAX_CENTIPAWN_DIFF:
# 			print("The difference is higher than the max centipawn diff!!!!!! ")
# 			print("Adjusting a move..." + " ADJUST! " * 10)
# 			print("Best move is " + str(stockFishMove) + " " + "#" * 20)
# 			allMoves.append(stockFishMove)
#
# 			board.push(chess.Move.from_uci(stockFishMove))
# 		else:
# 			print("GAN's score is accurate! " + ":D " * 50)
# 			allMoves.append(str(comp_move)[0:4])
# 			print("BEST MOVE IS " + str(str(comp_move)[0:4]) + " " + "$" * 20)
# 			board.push(comp_move)


		# prev variation
		# score, comp_move = negamax(board, 0, -1, float('-inf'), float('inf'), magikarp)
		# print("STGAN's score of a position is " + str(score))
		# board.push(comp_move)