import math

import pygame
from pygame.display import set_mode
from pygame.draw import rect
from pygame.locals import QUIT, KEYUP, K_ESCAPE, Rect
from sys import exit

from tkinter import Tk, messagebox

squareCenters = []
ScreenWidth = BoardWidth = 640
ScreenHeight = BoardHeight = 640
screen = set_mode((ScreenWidth, ScreenHeight))
light_brown = (238, 215, 181)
gray = (100, 100, 100)
violet = (238, 130, 238)
dark_brown = (178, 135, 101)
colors = [dark_brown, light_brown]



# def showCoordinates():
#     for i in range(0, len(squareCenters)):
#         print(squareCenters[i])
#     print("Chess pieces")
#     for i in range(0, len(Pieces)):
#         print(Pieces[i].team)


def initMap(cur):
    cur['a8'] = squareCenters[0]
    cur['a7'] = squareCenters[1]
    cur['a6'] = squareCenters[2]
    cur['a5'] = squareCenters[3]
    cur['a4'] = squareCenters[4]
    cur['a3'] = squareCenters[5]
    cur['a2'] = squareCenters[6]
    cur['a1'] = squareCenters[7]
    cur['b8'] = squareCenters[8]
    cur['b7'] = squareCenters[9]
    cur['b6'] = squareCenters[10]
    cur['b5'] = squareCenters[11]
    cur['b4'] = squareCenters[12]
    cur['b3'] = squareCenters[13]
    cur['b2'] = squareCenters[14]
    cur['b1'] = squareCenters[15]
    cur['c8'] = squareCenters[16]
    cur['c7'] = squareCenters[17]
    cur['c6'] = squareCenters[18]
    cur['c5'] = squareCenters[19]
    cur['c4'] = squareCenters[20]
    cur['c3'] = squareCenters[21]
    cur['c2'] = squareCenters[22]
    cur['c1'] = squareCenters[23]
    cur['d8'] = squareCenters[24]
    cur['d7'] = squareCenters[25]
    cur['d6'] = squareCenters[26]
    cur['d5'] = squareCenters[27]
    cur['d4'] = squareCenters[28]
    cur['d3'] = squareCenters[29]
    cur['d2'] = squareCenters[30]
    cur['d1'] = squareCenters[31]
    cur['e8'] = squareCenters[32]
    cur['e7'] = squareCenters[33]
    cur['e6'] = squareCenters[34]
    cur['e5'] = squareCenters[35]
    cur['e4'] = squareCenters[36]
    cur['e3'] = squareCenters[37]
    cur['e2'] = squareCenters[38]
    cur['e1'] = squareCenters[39]
    cur['f8'] = squareCenters[40]
    cur['f7'] = squareCenters[41]
    cur['f6'] = squareCenters[42]
    cur['f5'] = squareCenters[43]
    cur['f4'] = squareCenters[44]
    cur['f3'] = squareCenters[45]
    cur['f2'] = squareCenters[46]
    cur['f1'] = squareCenters[47]
    cur['g8'] = squareCenters[48]
    cur['g7'] = squareCenters[49]
    cur['g6'] = squareCenters[50]
    cur['g5'] = squareCenters[51]
    cur['g4'] = squareCenters[52]
    cur['g3'] = squareCenters[53]
    cur['g2'] = squareCenters[54]
    cur['g1'] = squareCenters[55]
    cur['h8'] = squareCenters[56]
    cur['h7'] = squareCenters[57]
    cur['h6'] = squareCenters[58]
    cur['h5'] = squareCenters[59]
    cur['h4'] = squareCenters[60]
    cur['h3'] = squareCenters[61]
    cur['h2'] = squareCenters[62]
    cur['h1'] = squareCenters[63]


def drawboard(_colors):
    increment = BoardWidth / 8
    index = 1  # toswitchcolors (index - 1) * -1
    for column in range(8):
        for row in range(8):
            Square = Rect(row * increment, column * increment, increment + 1, increment + 1)
            if Square not in squareCenters:
                squareCenters.append(Square)
            rect(screen, _colors[index], Square)
            index = (index - 1) * -1
        index = (index - 1) * -1


def make_lines(position, positionlist, anglelist):

    listofpossiblelines = []
    for Square in positionlist:
        for angle in anglelist:
            dx = Square.centerx - position.centerx
            dy = Square.centery - position.centery
            newangle = math.atan2(-dy, dx)
            if angle == newangle:
                listofpossiblelines.append([Square, angle])
    return listofpossiblelines


def square(x):
    return x * x


def distance_formula(pos1, pos2):
    # pos1 and pos2 are tuples of 2 numbers
     return math.sqrt(square(pos2[0] - pos1[0]) + square(pos2[1] - pos1[1]))


def isfarther(start, pos1, pos2):
    # Returns T/F whether pos2 is farther away than pos1

    if type(pos2) == int:  # for pawns
        return pos2 > distance_formula(start.center, pos1)
    else:
        return distance_formula(start.center, pos2) > distance_formula(start.center, pos1)




def nearest_piece(position, listof):
    nearest = None
    posCounter = 50000  # a very high number/ could use board dimension^2
    for piece in listof:
        if distance_formula(piece.rect.center, position) < posCounter:
            nearest = piece
            posCounter = distance_formula(piece.rect.center, position)

    if posCounter < BoardWidth / 8 - 30:
        return nearest  # only works when close
    else:
        print("returning none")
        return None

def show_checkmate(teams):
    Tk().wm_withdraw()  # to hide the main window
    messagebox.showinfo("CheckMate", teams[1] + " wins!")

def show_check(teams):
    Tk().wm_withdraw()  # to hide the main window
    messagebox.showinfo("Check!", teams[0] + "'s King under check")


def checkquitgame():
    for _ in pygame.event.get(QUIT):
        print(len(squareCenters))
        pygame.quit()
        exit()

    for event in pygame.event.get(KEYUP):
        if event.key == K_ESCAPE:
            pygame.quit()
            exit()
        pygame.event.post(event)